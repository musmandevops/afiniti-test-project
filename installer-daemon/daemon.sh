# Daemon to copy and delete files from remote server

# check if script already running
status=`ps -efww | grep -w "sftp-daemon.sh" | grep -v grep | grep -v $$ | awk '{ print $2 }'`
if [ ! -z "$status" ]; then
       echo "[`date`] : sftp-daemon.sh : Process is already running"
       exit 1;
fi

while true; do
	OUTPUT=$(rsync --remove-source-files -aht  -e "ssh -p $SSH_PORT" $SSH_USER@$SSH_ADDR:~/sftp_data  /root/ --out-format="%f")

	for file in $OUTPUT; do
		if [ "${file: -4}" == ".rpm" ]
		then
			echo "$(date) : intalling RPM Package $file"
			yum install $file --skip-broken
		elif [ "${file: -4}" == ".tar.gz" ]
		then
			echo "$(date) : loading image $file"
			docker load -i $file
		elif [ "${file: -4}" == ".yml" ]
		then
			echo "$(date) : checking if image for compose is already exist for compose $file"
			image_name=$(grep image $file | awk '{print$2}' | sed 's/"//g')
			if docker inspect $image_name >> /dev/null;
			then
				echo "docker image $image_name exist so running compose file"
				docker-compose -f $file up -d
			else
				echo "docker image $image_name not exist so exiting !!!!"
			fi
		fi
	done
	sleep 10
done
