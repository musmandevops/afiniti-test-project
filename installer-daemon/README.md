# Daemon Installer


### clone repo
`cd ~`
`git clone git@gitlab.com:musmandevops/installer-daemon.git`


### cd to directory

`cd installer-daemon`


## install docker and docker-compose on VM

### you can use provided ansible playbook to install docker on VM.

1. install ansible
2. change directory to ansible `cd ansible`
3. Edit hosts file and replace IP address by your VM.
3. ansible-playbooks install-docker-centos.yaml




## Set env variables for SFTP server
1. Set envoirnment variables by running below command. Do not forget to replace IP ADDRESS of SFTP VM with SFTP_VM_IP_ADDRESS.
2. `echo "export SSH_PORT=1759\nexport SSH_ADDR=SFTP_VM_IP_ADDRESS\nexport SSH_USER=root" >> root/.bashrc` 

## Private key file setup
1. Copy key file to $HOME/.ssh directory
2. `cp id_rsa $HOME/.ssh/`


## Create Systemd service to run every 10 seconds

1. Copy service and daemon files to systemd `cp sftp-daemon.service /etc/systemd/system/`
2. Reload daemon `systemctl daemon-reload`
3. Start daemon `systemctl start sftp-daemon.service`
4. Check status `systemctl status sftp-daemon.service`
5. Enable for restart `systemctl enable sftp-daemon.service`

# We can update sleep time in damon.sh script 