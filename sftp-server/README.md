# SFTP Server

### clone repo

`git clone git@gitlab.com:musmandevops/sftp-server.git`

### cd to directory

`cd sftp-server`

## install docker and docker-compose on VM

### you can use provided ansible playbook to install docker on VM.

1. install ansible
2. change directory to ansible `cd ansible`
3. Edit hosts file and replace IP address by your VM.
3. ansible-playbooks install-docker-ubuntu.yaml



## create docker image
create docker image by running command `docker build -t sftp:latest .`

## run sftp server container 
Finally run `docker-compose up -d` command.

This will create SFTP server on conatiner.
Some things to know before further processing.

1. SFTP server will listen on **PORT 1759** on host VM.
2. A volume sftp_data is mapped with host VM.
3. Change permission of key file `chmod 400 id_rsa`
4. Test connection by running `ssh -i id_rsa -p1759 root@127.0.0.1`
5. I have included ssh key files in this project but you can generate ssh keys by running command `ssh-keygen`.
6. You need to copy private key named id_rsa in Daemon directory if created by yourself